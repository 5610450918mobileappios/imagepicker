//
//  ViewController.swift
//  Image
//
//  Created by Teerapat on 9/29/2558 BE.
//  Copyright (c) 2558 Teerapat. All rights reserved.
//

import UIKit
import MobileCoreServices

class ViewController: UIViewController,UIImagePickerControllerDelegate,UINavigationControllerDelegate {
    
    var mediaList:[MediaSelected] = [MediaSelected]()
    
    var pageViewController:PageViewController!
    
    let imagePicker = UIImagePickerController()
    
    @IBOutlet weak var imageView: UIImageView!
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if(segue.identifier == "toPageViewContollerSegue"){
            let destination = segue.destinationViewController as! PageViewController
            destination.mediaList = self.mediaList
            self.pageViewController = destination
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        imagePicker.delegate = self
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func addImageButtonTough(sender: AnyObject) {
//        imagePicker.mediaTypes = [
        
        imagePicker.allowsEditing = false
        imagePicker.mediaTypes = UIImagePickerController.availableMediaTypesForSourceType(UIImagePickerControllerSourceType.PhotoLibrary)!
        imagePicker.sourceType = .PhotoLibrary
        presentViewController(imagePicker, animated: true, completion: nil)
    }

    @IBAction func removeImageButtonTough(sender: AnyObject) {
        if let pvc = self.pageViewController{
            
            // Remove current media
            
            let currentPage = pvc.currentPage
            
            if(currentPage >= 0){
                
                if mediaList.count > 1{
                    
                    // Remove data
                    
                    mediaList.removeAtIndex(currentPage)
                    
                    // Reset page
                    
                    pvc.mediaList = self.mediaList
                    
                    let index = min(currentPage, self.mediaList.count-1)
                    
                    pvc.startingIndex = index
                    
                    pvc.currentPage = index
                    
                    if let controller = pvc.createControllerAtIndex(index){
                        
                        pvc.setViewControllers([controller], direction: .Forward, animated: false, completion: nil)
                        
                        return
                        
                    }
                    
                }
                
            }
            
            // When fail or just single page reset everything
            
            self.mediaList = [MediaSelected]()
            
            pvc.mediaList = self.mediaList
            
            pvc.startingIndex = 0
            
            pvc.currentPage = 0
            
            pvc.initPageViewController()
            
        }
        
        
    }

    
    
    //Mark: - UIImagePickerControllerDelegate
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [NSObject : AnyObject]) {
        
        let mediaSelected = MediaSelected()
        
        let ImageType = kUTTypeImage as String
        let MovieType = kUTTypeMovie as String
        
        if let type: AnyObject = info[UIImagePickerControllerMediaType]{
            switch (type as! String){
            case ImageType:
                mediaSelected.mediaType = MediaType.IMAGE
                mediaSelected.image = (info[UIImagePickerControllerOriginalImage] as? UIImage)!
                
                imageView.image = mediaSelected.image
                break;
            case MovieType:
                mediaSelected.mediaType = MediaType.MOVIE
                mediaSelected.fileURL = (info[UIImagePickerControllerMediaURL] as? NSURL)!
                
                break;
            default:break;
                
            }
            addNewPage(mediaSelected)
        }
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    func addNewPage(media:MediaSelected){
        
        if let vc = self.pageViewController {
            
            mediaList.insert(media, atIndex: vc.currentPage)
            
            vc.mediaList = mediaList
            
            let index = vc.currentPage
            
            if let controller = vc.createControllerAtIndex(index) {
                
                vc.startingIndex = index
                
                vc.setViewControllers([controller], direction: .Forward, animated: false,
                    
                    completion: nil)
                
            }
            
        }else { mediaList.append(media) }
        
    }
}

