//
//  EmptyViewController.swift
//  Image
//
//  Created by Teerapat on 9/29/2558 BE.
//  Copyright (c) 2558 Teerapat. All rights reserved.
//

import UIKit

class EmptyViewController: UIViewController ,PageInfo{

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    //MARK: - PageInfo protocol
    internal var _pageIndex:Int = 0
    internal var pageIndex:Int{
        set(v){ self._pageIndex = v}
        get{ return self._pageIndex}
    }
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
