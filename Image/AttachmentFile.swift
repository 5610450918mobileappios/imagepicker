//
//  AttachmentFile.swift
//  Image
//
//  Created by Teerapat on 9/29/2558 BE.
//  Copyright (c) 2558 Teerapat. All rights reserved.
//

import Foundation
import UIKit
enum MediaType{
    case IMAGE
    case MOVIE
    case UNKNOW
}

class MediaSelected{
    var image:UIImage! //=UIImage()
    var fileURL:NSURL!//=NSURL()
    var mediaType:MediaType = MediaType.UNKNOW
    
    init(){}
}
