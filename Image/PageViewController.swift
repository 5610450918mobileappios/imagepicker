//
//  PageViewController.swift
//  Image
//
//  Created by Teerapat on 9/29/2558 BE.
//  Copyright (c) 2558 Teerapat. All rights reserved.
//

import UIKit
import MobileCoreServices
import AVFoundation
import AVKit

class PageViewController: UIPageViewController ,UIPageViewControllerDelegate,UIPageViewControllerDataSource{
    
    let proxy = UIPageControl.appearance()
    var startingIndex = 0
    var currentPage = 0
    var mediaList: [MediaSelected] = [MediaSelected]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.dataSource = self
        self.delegate = self
        initPageViewController()
        
        proxy.pageIndicatorTintColor = UIColor.grayColor()
        proxy.currentPageIndicatorTintColor = UIColor.greenColor()
        
    }
    func initPageViewController(){
        if(mediaList.count > 0) {
            
            if let controller = createControllerAtIndex(0){
                
                self.setViewControllers([controller], direction: .Forward, animated: false, completion: nil)
                
            }
            
        }else{
            
            self.setViewControllers([storyboard!.instantiateViewControllerWithIdentifier("EmptyViewController")],
                
                direction: .Forward,
                
                animated: false,
                
                completion: nil)
            
        }
        
    }
    
    func pageViewController(pageViewController: UIPageViewController, viewControllerAfterViewController viewController: UIViewController) -> UIViewController? {
        if (viewController.isMemberOfClass(MoviePlayerViewController) ||
            
            viewController.isMemberOfClass(ImageViewController)){
                
                let vc = viewController as! PageInfo
                
                let index = vc.pageIndex
                
                if(index < mediaList.count-1) {
                    
                    return createControllerAtIndex(index+1)
                    
                }
                
        }
        return nil
    }
    func pageViewController(pageViewController: UIPageViewController, viewControllerBeforeViewController viewController: UIViewController) -> UIViewController? {
        if (viewController.isMemberOfClass(MoviePlayerViewController) ||
            
            viewController.isMemberOfClass(ImageViewController)){
                
                let vc = viewController as! PageInfo
                
                let index = vc.pageIndex
                
                if(index > 0) {
                    
                    return createControllerAtIndex(index-1)
                    
                }
                
        }
        return nil
    }
    func pageViewController(pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [AnyObject], transitionCompleted completed: Bool) {
        let vc = pageViewController.viewControllers![0]
        
        if (vc.isMemberOfClass(MoviePlayerViewController) ||
            
            vc.isMemberOfClass(ImageViewController)){
                
                let pvc = vc as! PageInfo
                
                let index = pvc.pageIndex
                
                self.currentPage = index
                
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func presentationCountForPageViewController(pageViewController: UIPageViewController) -> Int {
        return mediaList.count
    }
    func presentationIndexForPageViewController(pageViewController: UIPageViewController) -> Int {
        return self.startingIndex
    }
    func createControllerAtIndex(index:Int) -> UIViewController!{
        
        if(mediaList.count < 1 || index >= mediaList.endIndex || index < 0) { return nil }
        
        let media:MediaSelected = mediaList[index]
        
        switch media.mediaType{
            
        case .IMAGE:
            
            let controller:ImageViewController = storyboard!.instantiateViewControllerWithIdentifier("ImageViewController") as! ImageViewController
            
            controller.image = media.image
            
            controller.pageIndex = index
            
            return controller
            
        case .MOVIE:
            
            let controller:MoviePlayerViewController = storyboard!.instantiateViewControllerWithIdentifier("MoviePlayerViewController") as!
                
            MoviePlayerViewController
            
            controller.player = AVPlayer(URL: media.fileURL)
            
            controller.pageIndex = index
            
            return controller
            
        default:
            
            return nil
            
        }
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
