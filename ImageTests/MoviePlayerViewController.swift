//
//  MoviePlayerViewController.swift
//  Image
//
//  Created by Teerapat on 10/6/2558 BE.
//  Copyright (c) 2558 Teerapat. All rights reserved.
//

import UIKit

import AVKit

import AVFoundation

class MoviePlayerViewController: AVPlayerViewController, PageInfo{
    
    //MARK: - PageInfo protocol
    
    internal var _pageIndex:Int = 0
    
    internal var pageIndex:Int {
        
        set(v){self._pageIndex = v}
        
        get{return self._pageIndex}
        
    }
    
}
