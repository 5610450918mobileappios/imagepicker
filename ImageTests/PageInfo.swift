//
//  PageInfo.swift
//  Image
//
//  Created by Teerapat on 10/6/2558 BE.
//  Copyright (c) 2558 Teerapat. All rights reserved.
//

import Foundation

protocol PageInfo{
    var pageIndex: Int{get set}
    
}